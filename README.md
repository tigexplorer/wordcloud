Moved to *codeberg*

# wordcloud


## Install

* Clone the repo `git clone https://gitlab.com/tigexplorer/wordcloud.git`
* Prevent accessing git files from the web, activate and edit the htaccess file
  ```
  mv htaccess.template .htaccess
  nano .htaccess
  ```
* Set admin access (creating .htusers_wordcloud with user/ password,
    replace my-web-path with yours path, replace myAdminUser with yours)
  `htpasswd -c my-web-path/.htusers_wordcloud myAdminUser`

* Create database
  ```
  mysql -u MYUSER -p
  create database `wordcloud`;
  grant CREATE,ALTER,SELECT,INSERT,UPDATE,DELETE,INDEX,DROP on `wordcloud`.* to 'MyWordcloudUSER'@'localhost' identified by 'MyPassword';
  SHOW databases;
  SELECT User FROM mysql.user;

  ```

* Create tables
  ```
  USE wordcloud;
  CREATE TABLE IF NOT EXISTS words (
      word_id INT AUTO_INCREMENT,
      word VARCHAR(255) NOT NULL,
      PRIMARY KEY (word_id)
  )  ENGINE=INNODB;

  SHOW TABLES;
  ```

* Set db access
  ```
  cp assets/php/db_conf_pdo.inc.php.template assets/php/db_conf_pdo.inc.php
  nano assets/php/db_conf_pdo.inc.php
  ```
