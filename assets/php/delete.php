<?php
require "db_conf_pdo.inc.php";
ini_set('display_errors', 'On');

if (!isset($_POST['w_auth'])){
    echo "Access forbidden!";
    exit;
} else {
    $w_auth=$_POST['w_auth'];
}

//if ($w_auth != ""){
if ($w_auth != $auth_string){
    echo "No auth! Access forbidden!";
    exit;
}

if (isset($_POST['w_spec'])){
    $w_spec=$_POST['w_spec'];
    if ($w_spec !=""){
        $sql="DELETE FROM words WHERE word='$w_spec'";
    } else {
        echo "No word was specified, nothing deleted!";
        exit;
    }
} else {
    $sql="DELETE FROM words";
}

$db = db_connect_pdo();
$count = $db->exec($sql);

if ($count !== false){
    echo "Affected rows: ". $count; // Shows the number of aAffected rows
} else {
    echo "Error while deleting...";
}

?>
